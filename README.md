# Tema Kubernetes

## Deployment si Service nginx:

### Fisierul: _nginx.yml_

```
kubectl apply -f nginx.yml
kubectl get services
kubectl get deployments
kubectl logs services/nginx-service
kubectl logs deployments/nginx-deployment
```

## Pentru Job si Cronjob

### Fisierele: _job.yaml_ si _cronjob.yaml_

```
kubectl apply -f job.yml
kubectl apply -f cronjob.yaml

kubectl get jobs
kubectl get cronjobs

kubectl logs jobs/printenv-job
kubectl logs cronjobs/printenv-cronjob
```

## Pod-ul care afiseaza din secret

### Fisierele: _secret.yaml_ si _secret-pod.yaml_

```
kubectl apply -f secret.yaml
kubectl get secrets

kubectl apply -f secret-pod.yaml
kubectl logs pods/secret-pod
```

## Pod-ul care afiseaza din configmap

### Fisierele: _configmap.yaml_ si _configmap-pod.yaml_

```
kubectl apply -f configmap.yaml
kubectl get configmap

kubectl apply -f configmap-pod.yaml
kubectl logs pods/configmap-pod
```
